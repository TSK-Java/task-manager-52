package ru.tsc.kirillov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.api.repository.ICommandRepository;
import ru.tsc.kirillov.tm.api.service.ICommandService;
import ru.tsc.kirillov.tm.command.AbstractCommand;

import java.util.Collection;

public class CommandService implements ICommandService {

    @NotNull
    private final ICommandRepository commandRepository;

    public CommandService(@NotNull ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }

    @Override
    public void add(@Nullable final AbstractCommand command) {
        commandRepository.add(command);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByName(@Nullable final String name) {
        if (name == null || name.isEmpty())
            return null;
        return commandRepository.getCommandByName(name);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByArgument(@Nullable final String argument) {
        return commandRepository.getCommandByArgument(argument);
    }

    @NotNull
    @Override
    public Iterable<AbstractCommand> getCommandsWithArgument() {
        return commandRepository.getCommandsWithArgument();
    }

}
