package ru.tsc.kirillov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.dto.request.ProjectBindTaskByIdRequest;
import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class ProjectBindTaskByIdCommand extends AbstractProjectTaskCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-bind-task-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Привязать задачу к проекту.";
    }

    @Override
    public void execute() {
        System.out.println("[Привязка задачи к проекту]");
        System.out.println("Введите ID проекта:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("Введите ID задачи:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @NotNull final ProjectBindTaskByIdRequest request = new ProjectBindTaskByIdRequest(getToken(), projectId, taskId);
        getProjectTaskEndpoint().bindTaskToProjectId(request);
    }

}
