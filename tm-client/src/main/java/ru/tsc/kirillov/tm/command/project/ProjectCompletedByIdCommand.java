package ru.tsc.kirillov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.dto.request.ProjectChangeStatusByIdRequest;
import ru.tsc.kirillov.tm.enumerated.Status;
import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class ProjectCompletedByIdCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-completed-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Завершить проект по его ID.";
    }

    @Override
    public void execute() {
        System.out.println("Завершение проекта по ID]");
        System.out.println("Введите ID проекта:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectChangeStatusByIdRequest request =
                new ProjectChangeStatusByIdRequest(getToken(), id, Status.COMPLETED);
        getProjectEndpoint().changeProjectStatusById(request);
    }

}
