package ru.tsc.kirillov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.dto.request.TaskGetByIndexRequest;
import ru.tsc.kirillov.tm.dto.response.TaskGetByIndexResponse;
import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskShowCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-show-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Отобразить задачу по её индексу.";
    }

    @Override
    public void execute() {
        System.out.println("[Отображение задачи по индексу]");
        System.out.println("Введите индекс задачи:");
        @NotNull final Integer index = TerminalUtil.nextNumber();
        @NotNull final TaskGetByIndexRequest request = new TaskGetByIndexRequest(getToken(), index);
        @NotNull final TaskGetByIndexResponse response = getTaskEndpoint().getTaskByIndex(request);
        showTask(response.getTask());
    }

}
