package ru.tsc.kirillov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.dto.request.ProjectUpdateByIdRequest;
import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-update-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Обновить проект по его ID.";
    }

    @Override
    public void execute() {
        System.out.println("[Обновление проекта по ID]");
        System.out.println("Введите ID проекта:");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("Введите имя:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Введите описание:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(getToken(), id, name, description);
        getProjectEndpoint().updateProjectById(request);
    }

}
