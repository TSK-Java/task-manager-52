package ru.tsc.kirillov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.dto.request.UserRemoveRequest;
import ru.tsc.kirillov.tm.enumerated.Role;
import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class UserRemoveCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-remove";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Удаление пользователя.";
    }

    @Override
    public void execute() {
        System.out.println("[Удаление пользователя]");
        System.out.println("Введите логин пользователя:");
        @NotNull final String login = TerminalUtil.nextLine();
        @NotNull final UserRemoveRequest request = new UserRemoveRequest(getToken(), login);
        getUserEndpoint().removeUser(request);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMIN };
    }

}
