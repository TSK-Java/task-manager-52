package ru.tsc.kirillov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.dto.request.DataXmlFasterXmlLoadRequest;
import ru.tsc.kirillov.tm.enumerated.Role;

public final class DataLoadXmlFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String getName() {
        return "data-load-xml-fasterxml";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Загрузить состояние приложения из xml файла (FasterXML API)";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[Загрузка состояния приложения из xml файла (FasterXML API)]");
        getDomainEndpoint().loadDataXmlFasterXml(new DataXmlFasterXmlLoadRequest(getToken()));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
