package ru.tsc.kirillov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.dto.request.UserLockRequest;
import ru.tsc.kirillov.tm.enumerated.Role;
import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class UserLockCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-lock";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Блокировка пользователя.";
    }

    @Override
    public void execute() {
        System.out.println("[Блокировка пользователя]");
        System.out.println("Введите логин пользователя:");
        @NotNull final String login = TerminalUtil.nextLine();
        getUserEndpoint().lockUser(new UserLockRequest(getToken(), login));
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMIN };
    }

}
