package ru.tsc.kirillov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lombok.SneakyThrows;
import org.bson.Document;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.api.ILoggerService;

import java.util.LinkedHashMap;
import java.util.Map;

public final class LoggerService implements ILoggerService {

    @NotNull
    private static final String MONGO_HOST = "localhost";

    private static final int MONGO_PORT = 27017;

    @NotNull
    private static final String MONGO_DB_NAME = "tm_log";

    @NotNull
    private static final String FIELD_TABLE_NAME = "table";

    @NotNull
    private final ObjectMapper objectMapper = new ObjectMapper();

    @NotNull
    private final MongoClient client = new MongoClient(MONGO_HOST, MONGO_PORT);

    @NotNull
    private final MongoDatabase database = client.getDatabase(MONGO_DB_NAME);

    @Override
    @SneakyThrows
    public void writeLog(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        @NotNull final Map<String, Object> event = objectMapper.readValue(message, LinkedHashMap.class);
        @Nullable final String tableName = event.get(FIELD_TABLE_NAME).toString();
        if (tableName == null || tableName.isEmpty()) return;
        if (database.getCollection(tableName) == null) database.createCollection(tableName);
        @NotNull final MongoCollection<Document> collection = database.getCollection(tableName);
        collection.insertOne(new Document(event));
    }

}
