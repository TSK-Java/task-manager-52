package ru.tsc.kirillov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.kirillov.tm.api.service.model.IProjectService;
import ru.tsc.kirillov.tm.api.service.model.IProjectTaskService;
import ru.tsc.kirillov.tm.api.service.model.ITaskService;
import ru.tsc.kirillov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kirillov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kirillov.tm.exception.field.ProjectIdEmptyException;
import ru.tsc.kirillov.tm.exception.field.TaskIdEmptyException;
import ru.tsc.kirillov.tm.exception.field.UserIdEmptyException;
import ru.tsc.kirillov.tm.model.Project;
import ru.tsc.kirillov.tm.model.Task;

import java.util.UUID;

public final class ProjectTaskServiceTest extends AbstractUserTest {
    
    @NotNull 
    private IProjectService projectService;

    @NotNull
    private ITaskService taskService;

    @NotNull
    private IProjectTaskService projectTaskService;
    
    @NotNull 
    private final String projectName = UUID.randomUUID().toString();

    @NotNull 
    private final String taskName = UUID.randomUUID().toString();

    @Before
    @Override
    public void initialization() {
        super.initialization();
        projectService = new ProjectService(CONNECTION_SERVICE);
        taskService = new TaskService(CONNECTION_SERVICE);
        projectTaskService = new ProjectTaskService(projectService, taskService);
    }

    @After
    @Override
    public void finalization() {
        taskService.clear();
        projectService.clear();
        super.finalization();
    }

    @Test
    public void bindTaskToProject() {
        @Nullable Project project = projectService.create(userId, projectName);
        Assert.assertNotNull(project);
        @Nullable Task task = taskService.create(userId, taskName);
        Assert.assertNotNull(task);
        @Nullable Project projectRandom = projectService.create(userAdmin.getId(), projectName);
        Assert.assertNotNull(projectRandom);
        @Nullable Task taskRandom = taskService.create(userAdmin.getId(), taskName);
        Assert.assertNotNull(taskRandom);
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject("", "", "")
        );
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(null, null, null)
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(userId, "", "")
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(userId, null, null)
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(userId, project.getId(), "")
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(userId, project.getId(), null)
        );
        Assert.assertThrows(
                ProjectNotFoundException.class,
                () -> projectTaskService.bindTaskToProject(userId, projectRandom.getId(), taskRandom.getId())
        );
        Assert.assertThrows(
                TaskNotFoundException.class,
                () -> projectTaskService.bindTaskToProject(userId, project.getId(), taskRandom.getId())
        );
        Assert.assertNull(task.getProject());
        projectTaskService.bindTaskToProject(userId, project.getId(), task.getId());
        task = taskService.findOneById(userId, task.getId());
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getProject());
        Assert.assertEquals(project.getId(), task.getProject().getId());
    }

    @Test
    public void unbindTaskToProject() {
        @Nullable Project project = projectService.create(userId, projectName);
        Assert.assertNotNull(project);
        @Nullable Task task = taskService.create(userId, taskName);
        Assert.assertNotNull(task);
        @Nullable Project projectRandom = projectService.create(userAdmin.getId(), projectName);
        Assert.assertNotNull(projectRandom);
        @Nullable Task taskRandom = taskService.create(userAdmin.getId(), taskName);
        Assert.assertNotNull(taskRandom);
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectTaskService.unbindTaskToProject("", "", "")
        );
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectTaskService.unbindTaskToProject(null, null, null)
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> projectTaskService.unbindTaskToProject(userId, "", "")
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> projectTaskService.unbindTaskToProject(userId, null, null)
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> projectTaskService.unbindTaskToProject(userId, project.getId(), "")
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> projectTaskService.unbindTaskToProject(userId, project.getId(), null)
        );
        Assert.assertThrows(
                ProjectNotFoundException.class,
                () -> projectTaskService.unbindTaskToProject(userId, projectRandom.getId(), taskRandom.getId())
        );
        Assert.assertThrows(
                TaskNotFoundException.class,
                () -> projectTaskService.unbindTaskToProject(userId, project.getId(), taskRandom.getId())
        );
        task.setProject(project);
        taskService.update(task);
        task = taskService.findOneById(userId, task.getId());
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getProject());
        Assert.assertEquals(project.getId(), task.getProject().getId());
        projectTaskService.unbindTaskToProject(userId, project.getId(), task.getId());
        task = taskService.findOneById(userId, task.getId());
        Assert.assertNotNull(task);
        Assert.assertNull(task.getProject());
    }

    @Test
    public void removeProjectById() {
        @Nullable Project project = projectService.create(userId, projectName);
        Assert.assertNotNull(project);
        @Nullable Task task = taskService.create(userId, taskName);
        Assert.assertNotNull(task);
        projectService.create(userAdmin.getId(), projectName);
        taskService.create(userAdmin.getId(), taskName);
        task.setProject(project);
        taskService.update(task);
        task = taskService.findOneById(userId, task.getId());
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getProject());
        Assert.assertEquals(project.getId(), task.getProject().getId());
        Assert.assertEquals(2, taskService.count());
        Assert.assertEquals(2, projectService.count());
        Assert.assertEquals(1, taskService.count(userId));
        Assert.assertEquals(1, projectService.count(userId));
        Assert.assertThrows(
                UserIdEmptyException.class, () -> projectTaskService.removeProjectById("", "")
        );
        Assert.assertThrows(
                UserIdEmptyException.class, () -> projectTaskService.removeProjectById(null, null)
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class, () -> projectTaskService.removeProjectById(userId, "")
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class, () -> projectTaskService.removeProjectById(userId, null)
        );
        projectTaskService.removeProjectById(userId, project.getId());
        Assert.assertEquals(1, taskService.count());
        Assert.assertEquals(1, projectService.count());
        Assert.assertEquals(0, taskService.count(userId));
        Assert.assertEquals(0, projectService.count(userId));
    }

    @Test
    public void removeProjectByIndex() {
        @Nullable Project project = projectService.create(userId, projectName);
        Assert.assertNotNull(project);
        @Nullable Task task = taskService.create(userId, taskName);
        Assert.assertNotNull(task);
        projectService.create(userAdmin.getId(), projectName);
        taskService.create(userAdmin.getId(), taskName);
        task.setProject(project);
        taskService.update(task);
        task = taskService.findOneById(userId, task.getId());
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getProject());
        Assert.assertEquals(project.getId(), task.getProject().getId());
        Assert.assertEquals(2, taskService.count());
        Assert.assertEquals(2, projectService.count());
        Assert.assertEquals(1, taskService.count(userId));
        Assert.assertEquals(1, projectService.count(userId));
        Assert.assertThrows(
                UserIdEmptyException.class, () -> projectTaskService.removeProjectByIndex("", 1)
        );
        Assert.assertThrows(
                UserIdEmptyException.class, () -> projectTaskService.removeProjectByIndex(null, null)
        );
        Assert.assertThrows(
                IndexOutOfBoundsException.class, () -> projectTaskService.removeProjectByIndex(userId, null)
        );
        Assert.assertThrows(
                ProjectNotFoundException.class,
                () -> projectTaskService.removeProjectByIndex(UUID.randomUUID().toString(), 1)
        );
        Assert.assertThrows(
                IndexOutOfBoundsException.class, () -> projectTaskService.removeProjectByIndex(userId, -50)
        );
        Assert.assertThrows(
                ProjectNotFoundException.class, () -> projectTaskService.removeProjectByIndex(userId, 50)
        );
        projectTaskService.removeProjectByIndex(userId, 0);
        Assert.assertEquals(1, taskService.count());
        Assert.assertEquals(1, projectService.count());
        Assert.assertEquals(0, taskService.count(userId));
        Assert.assertEquals(0, projectService.count(userId));
    }

    @Test
    public void clear() {
        @Nullable Project project = projectService.create(userId, projectName);
        Assert.assertNotNull(project);
        @Nullable Task task = taskService.create(userId, taskName);
        Assert.assertNotNull(task);
        projectService.create(userAdmin.getId(), projectName);
        taskService.create(userAdmin.getId(), taskName);
        task.setProject(project);
        taskService.update(task);
        task = taskService.findOneById(userId, task.getId());
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getProject());
        Assert.assertEquals(project.getId(), task.getProject().getId());
        Assert.assertEquals(2, taskService.count());
        Assert.assertEquals(2, projectService.count());
        Assert.assertEquals(1, taskService.count(userId));
        Assert.assertEquals(1, projectService.count(userId));
        Assert.assertThrows(
                UserIdEmptyException.class, () -> projectTaskService.clear("")
        );
        Assert.assertThrows(
                UserIdEmptyException.class, () -> projectTaskService.clear(null)
        );
        projectTaskService.clear(userId);
        Assert.assertEquals(1, taskService.count());
        Assert.assertEquals(1, projectService.count());
        Assert.assertEquals(0, taskService.count(userId));
        Assert.assertEquals(0, projectService.count(userId));
    }

}
