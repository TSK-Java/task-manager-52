package ru.tsc.kirillov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.kirillov.tm.api.service.model.IProjectService;
import ru.tsc.kirillov.tm.enumerated.Sort;
import ru.tsc.kirillov.tm.enumerated.Status;
import ru.tsc.kirillov.tm.exception.entity.EntityNotFoundException;
import ru.tsc.kirillov.tm.exception.field.*;
import ru.tsc.kirillov.tm.exception.system.IndexOutOfBoundsException;
import ru.tsc.kirillov.tm.exception.user.UserNotFoundException;
import ru.tsc.kirillov.tm.model.Project;
import ru.tsc.kirillov.tm.model.User;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public final class ProjectServiceTest extends AbstractUserTest {
    
    @NotNull
    private IProjectService service;
    
    @NotNull
    private final String projectName = UUID.randomUUID().toString();

    @NotNull
    private final String projectDescription = UUID.randomUUID().toString();

    @NotNull
    private final Date dateBegin = new Date();

    @NotNull
    private final Date dateEnd = new Date();

    @NotNull
    private final String projectNewName = UUID.randomUUID().toString();

    @NotNull
    private final String projectNewDescription = UUID.randomUUID().toString();

    @NotNull
    private final Status projectStatus = Status.COMPLETED;

    @Before
    @Override
    public void initialization() {
        super.initialization();
        service = new ProjectService(CONNECTION_SERVICE);
    }

    @After
    @Override
    public void finalization() {
        service.clear();
        super.finalization();
    }

    @Test
    public void add() {
        Assert.assertEquals(0, service.count());
        @NotNull final Project project = new Project(userTest, projectName, projectDescription);
        @Nullable Project projectAdd = service.add(project);
        Assert.assertNotNull(projectAdd);
        Assert.assertEquals(project.getId(), projectAdd.getId());
        Assert.assertEquals(1, service.count());
        Assert.assertEquals(1, service.count(userId));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.count(""));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.count(null));
    }

    @Test
    public void clear() {
        Assert.assertEquals(0, service.count());
        service.create(userId, projectName);
        Assert.assertEquals(1, service.count());
        service.clear();
        Assert.assertEquals(0, service.count());
        service.create(userId, projectName);
        service.create(userAdmin.getId(), projectName);
        Assert.assertEquals(1, service.count(userId));
        Assert.assertEquals(0, service.count(UUID.randomUUID().toString()));
        service.clear(userId);
        Assert.assertEquals(1, service.count());
        Assert.assertEquals(0, service.count(userId));
        Assert.assertEquals(0, service.count(UUID.randomUUID().toString()));
    }

    @Test
    public void findAll() {
        Assert.assertEquals(0, service.count());
        service.create(userId, projectName);
        Assert.assertEquals(1, service.count());
        Assert.assertEquals(service.count(), service.findAll().size());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll(""));
        @Nullable final String userIdNull = null;
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll(userIdNull));
        @NotNull List<Project> projects = service.findAll(userId);
        Assert.assertEquals(service.count(), projects.size());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll(userIdNull, Sort.BY_NAME.getComparator()).size());
        service.clear();
        final int countProject = 10;
        for (int i = 0; i < countProject; i++) {
            @NotNull final String projectName = String.format("Project_%d", countProject - i - 1);
            service.create(userId, projectName);
        }
        projects = service.findAll(userId, Sort.BY_NAME.getComparator());
        Assert.assertEquals(countProject, projects.size());
        for (int i = 0; i < countProject; i++) {
            @NotNull final String projectName = String.format("Project_%d", i);
            Assert.assertEquals(projectName, projects.get(i).getName());
        }
        projects = service.findAll(userId, Sort.BY_NAME);
        Assert.assertEquals(countProject, projects.size());
        for (int i = 0; i < countProject; i++) {
            @NotNull final String projectName = String.format("Project_%d", i);
            Assert.assertEquals(projectName, projects.get(i).getName());
        }
    }

    @Test
    public void existsById() {
        Assert.assertEquals(0, service.count());
        @Nullable final Project project = service.create(userId, projectName);
        Assert.assertNotNull(project);
        Assert.assertTrue(service.existsById(project.getId()));
        Assert.assertFalse(service.existsById(UUID.randomUUID().toString()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.existsById(null, project.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.existsById("", project.getId()));
        Assert.assertFalse(service.existsById(UUID.randomUUID().toString(), project.getId()));
        Assert.assertTrue(service.existsById(userId, project.getId()));
        Assert.assertFalse(service.existsById(userId, UUID.randomUUID().toString()));
        Assert.assertThrows(IdEmptyException.class, () -> service.existsById(userId, ""));
        Assert.assertThrows(IdEmptyException.class, () -> service.existsById(userId, null));
    }

    @Test
    public void findOneById() {
        Assert.assertEquals(0, service.count());
        @Nullable final Project project = service.create(userId, projectName);
        Assert.assertNotNull(project);
        Assert.assertNotNull(service.findOneById(project.getId()));
        Assert.assertNull(service.findOneById(UUID.randomUUID().toString()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findOneById(null, project.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findOneById("", project.getId()));
        Assert.assertNull(service.findOneById(UUID.randomUUID().toString(), project.getId()));
        Assert.assertNotNull(service.findOneById(userId, project.getId()));
        Assert.assertNull(service.findOneById(userId, UUID.randomUUID().toString()));
        Assert.assertThrows(IdEmptyException.class, () -> service.findOneById(userId, ""));
        Assert.assertThrows(IdEmptyException.class, () -> service.findOneById(userId, null));
        @Nullable Project projectFind = service.findOneById(userId, project.getId());
        Assert.assertNotNull(projectFind);
        Assert.assertEquals(project.getId(), projectFind.getId());
    }

    @Test
    public void findOneByIndex() {
        Assert.assertEquals(0, service.count());
        @Nullable final Project project = service.create(userId, projectName);
        Assert.assertNotNull(project);
        @Nullable Project projectFind = service.findOneByIndex(0);
        Assert.assertNotNull(projectFind);
        Assert.assertEquals(project.getId(), projectFind.getId());
        projectFind = service.findOneByIndex(userId,0);
        Assert.assertNotNull(projectFind);
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findOneByIndex(null, 0));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findOneByIndex("", 0));
        Assert.assertThrows(IndexOutOfBoundsException.class, () -> service.findOneByIndex(userId, -50));
        Assert.assertNull(service.findOneByIndex(UUID.randomUUID().toString(), 0));
    }

    @Test
    public void remove() {
        Assert.assertEquals(0, service.count());
        @Nullable Project project = service.create(userId, projectName);
        Assert.assertNotNull(project);
        @Nullable Project projectRemove = service.remove(project);
        Assert.assertNotNull(projectRemove);
        Assert.assertEquals(0, service.count());
        Assert.assertEquals(project.getId(), projectRemove.getId());
        project = service.create(userId, projectName);
        Assert.assertNotNull(project);
        service.create(userAdmin.getId(), projectName);
        Assert.assertEquals(2, service.count());
        Assert.assertEquals(1, service.count(userId));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.remove(null, null));
        @Nullable final Project projectFinal = project;
        Assert.assertThrows(UserIdEmptyException.class, () -> service.remove(null, projectFinal));
        Assert.assertNull(service.remove(userId, null));
        Assert.assertEquals(2, service.count());
        Assert.assertNull(service.remove(UUID.randomUUID().toString(), project));
        Assert.assertEquals(2, service.count());
        projectRemove = service.remove(userId, project);
        Assert.assertNotNull(projectRemove);
        Assert.assertEquals(project.getId(), projectRemove.getId());
        Assert.assertEquals(1, service.count());
        Assert.assertEquals(0, service.count(userId));
    }

    @Test
    public void removeById() {
        Assert.assertEquals(0, service.count());
        @Nullable Project project = service.create(userId, projectName);
        Assert.assertNotNull(project);
        Assert.assertThrows(EntityNotFoundException.class, () -> service.removeById(UUID.randomUUID().toString()));
        Assert.assertEquals(1, service.count());
        @Nullable Project projectRemove = service.removeById(project.getId());
        Assert.assertNotNull(projectRemove);
        Assert.assertEquals(project.getId(), projectRemove.getId());
        Assert.assertEquals(0, service.count());
        project = service.create(userId, projectName);
        Assert.assertNotNull(project);
        service.create(userAdmin.getId(), projectName);
        Assert.assertEquals(2, service.count());
        Assert.assertEquals(1, service.count(userId));
        @NotNull final Project projectRemoveFinal = project;
        Assert.assertThrows(
                EntityNotFoundException.class,
                () -> service.removeById(UUID.randomUUID().toString(), projectRemoveFinal.getId())
        );
        Assert.assertEquals(2, service.count());
        Assert.assertThrows(
                EntityNotFoundException.class,
                () -> service.removeById(UUID.randomUUID().toString(), UUID.randomUUID().toString())
        );
        Assert.assertEquals(2, service.count());
        Assert.assertThrows(
                EntityNotFoundException.class,
                () -> service.removeById(userId, UUID.randomUUID().toString())
        );
        Assert.assertEquals(2, service.count());
        @NotNull final Project projectFinal = project;
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeById(null, projectFinal.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeById("", projectFinal.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(userId, null));
        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(userId, ""));
        projectRemove = service.removeById(userId, project.getId());
        Assert.assertNotNull(projectRemove);
        Assert.assertEquals(project.getId(), projectRemove.getId());
        Assert.assertEquals(1, service.count());
        Assert.assertEquals(0, service.count(userId));
    }

    @Test
    public void removeByIndex() {
        Assert.assertEquals(0, service.count());
        @Nullable Project project = service.create(userId, projectName);
        Assert.assertNotNull(project);
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeByIndex(null, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeByIndex("", null));
        Assert.assertThrows(IndexOutOfBoundsException.class, () -> service.removeByIndex(userId, null));
        Assert.assertThrows(IndexOutOfBoundsException.class, () -> service.removeByIndex(userId, -50));
        Assert.assertNull(service.removeByIndex(userId, 50));
        Assert.assertNull(service.removeByIndex(UUID.randomUUID().toString(), 0));
        @Nullable Project projectRemove = service.removeByIndex(userId, 0);
        Assert.assertNotNull(projectRemove);
        Assert.assertEquals(0, service.count());
        Assert.assertEquals(project.getId(), projectRemove.getId());
    }

    @Test
    public void count() {
        Assert.assertEquals(0, service.count());
        final int countProject = 10;
        for (int i = 0; i < countProject; i++) {
            service.create(userId, UUID.randomUUID().toString());
            service.create(userAdmin.getId(), UUID.randomUUID().toString());
            Assert.assertEquals(i*2 + 2, service.count());
            long cnt = service.count(userId);
            Assert.assertEquals(i + 1, cnt);
            Assert.assertEquals(service.count(), service.findAll().size());
            Assert.assertEquals(service.count(userId), service.findAll(userId).size());
        }
    }

    @Test
    public void create() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.create("", ""));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(userId, ""));
        @Nullable final User userNull = null;
        Assert.assertThrows(UserNotFoundException.class, () -> service.create(userNull, ""));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(userId, null));
        Assert.assertEquals(0, service.count());
        @Nullable Project project = service.create(userId, projectName);
        Assert.assertNotNull(project);
        Assert.assertEquals(1, service.count());
        @Nullable Project projectFind = service.findOneById(userId, project.getId());
        Assert.assertNotNull(projectFind);
        Assert.assertEquals(project.getName(), projectFind.getName());
    }

    @Test
    public void createDescription() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.create("", "", ""));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(userId, "", ""));
        Assert.assertThrows(DescriptionEmptyException.class, () -> service.create(userId, projectName, ""));
        @Nullable final User userNull = null;
        Assert.assertThrows(UserNotFoundException.class, () -> service.create(userNull, "", ""));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(userId, null, null));
        Assert.assertEquals(0, service.count());
        @Nullable Project project = service.create(userId, projectName, projectDescription);
        Assert.assertNotNull(project);
        Assert.assertEquals(1, service.count());
        @Nullable Project projectFind = service.findOneById(userId, project.getId());
        Assert.assertNotNull(projectFind);
        Assert.assertEquals(project.getName(), projectFind.getName());
        Assert.assertEquals(project.getDescription(), projectFind.getDescription());
    }

    @Test
    public void createDate() {
        Assert.assertEquals(0, service.count());
        @Nullable Project project = service.create(userId, projectName, projectDescription, dateBegin, dateEnd);
        Assert.assertNotNull(project);
        Assert.assertEquals(1, service.count());
        @Nullable Project projectFind = service.findOneById(userId, project.getId());
        Assert.assertNotNull(projectFind);
        Assert.assertEquals(project.getId(), projectFind.getId());
        Assert.assertEquals(projectName, projectFind.getName());
        Assert.assertEquals(projectDescription, projectFind.getDescription());
        Assert.assertEquals(dateBegin, projectFind.getDateBegin());
        Assert.assertEquals(dateEnd, projectFind.getDateEnd());
    }

    @Test
    public void updateById() {
        @Nullable Project project = service.create(userId, projectName, projectDescription, dateBegin, dateEnd);
        Assert.assertNotNull(project);
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.updateById("", "", "", "")
        );
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.updateById(null, null, null, null)
        );
        Assert.assertThrows(
                IdEmptyException.class,
                () -> service.updateById(userId, "", "", "")
        );
        Assert.assertThrows(
                IdEmptyException.class,
                () -> service.updateById(userId, null, null, null)
        );
        Assert.assertThrows(
                NameEmptyException.class,
                () -> service.updateById(userId, project.getId(), "", "")
        );
        Assert.assertThrows(
                NameEmptyException.class,
                () -> service.updateById(userId, project.getId(), null, null)
        );
        Assert.assertThrows(
                DescriptionEmptyException.class,
                () -> service.updateById(userId, project.getId(), projectNewName, "")
        );
        Assert.assertThrows(
                DescriptionEmptyException.class,
                () -> service.updateById(userId, project.getId(), projectNewName, null)
        );
        Assert.assertThrows(
                EntityNotFoundException.class,
                () -> service.updateById(userId, UUID.randomUUID().toString(), projectNewName, projectNewDescription)
        );
        Assert.assertThrows(
                EntityNotFoundException.class,
                () -> service.updateById(
                        UUID.randomUUID().toString(), project.getId(), projectNewName, projectNewDescription
                )
        );
        @Nullable Project projectUpdated =
                service.updateById(userId, project.getId(), projectNewName, projectNewDescription);
        Assert.assertNotNull(projectUpdated);
        Assert.assertEquals(project.getId(), projectUpdated.getId());
        Assert.assertEquals(projectNewName, projectUpdated.getName());
        Assert.assertEquals(projectNewDescription, projectUpdated.getDescription());
    }

    @Test
    public void updateByIndex() {
        @Nullable Project project = service.create(userId, projectName, projectDescription, dateBegin, dateEnd);
        Assert.assertNotNull(project);
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.updateByIndex("", null, "", "")
        );
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.updateByIndex(null, null, null, null)
        );
        Assert.assertThrows(
                IndexOutOfBoundsException.class,
                () -> service.updateByIndex(userId, null, "", "")
        );
        Assert.assertThrows(
                IndexOutOfBoundsException.class,
                () -> service.updateByIndex(userId, null, null, null)
        );
        Assert.assertThrows(
                IndexOutOfBoundsException.class,
                () -> service.updateByIndex(userId, -50, "", "")
        );
        Assert.assertThrows(
                IndexOutOfBoundsException.class,
                () -> service.updateByIndex(userId, -50, null, null)
        );
        Assert.assertThrows(
                NameEmptyException.class,
                () -> service.updateByIndex(userId, 0, "", "")
        );
        Assert.assertThrows(
                NameEmptyException.class,
                () -> service.updateByIndex(userId, 0, null, null)
        );
        Assert.assertThrows(
                DescriptionEmptyException.class,
                () -> service.updateByIndex(userId, 0, projectNewName, "")
        );
        Assert.assertThrows(
                DescriptionEmptyException.class,
                () -> service.updateByIndex(userId, 0, projectNewName, null)
        );
        Assert.assertThrows(
                EntityNotFoundException.class,
                () -> service.updateByIndex(UUID.randomUUID().toString(), 0, projectNewName, projectNewDescription)
        );
        @Nullable Project projectUpdated = service.updateByIndex(userId, 0, projectNewName, projectNewDescription);
        Assert.assertNotNull(projectUpdated);
        Assert.assertEquals(projectNewName, projectUpdated.getName());
        Assert.assertEquals(projectNewDescription, projectUpdated.getDescription());
    }

    @Test
    public void changeStatusByIdAllEmpty() {
        @Nullable Project project = service.create(userId, projectName, projectDescription, dateBegin, dateEnd);
        Assert.assertNotNull(project);
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.changeStatusById("", "", null)
        );
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.changeStatusById(null, null, null)
        );
        Assert.assertThrows(
                IdEmptyException.class,
                () -> service.changeStatusById(userId, "", null)
        );
        Assert.assertThrows(
                IdEmptyException.class,
                () -> service.changeStatusById(userId, null, null)
        );
        Assert.assertThrows(
                StatusEmptyException.class,
                () -> service.changeStatusById(userId, project.getId(), null)
        );
        Assert.assertThrows(
                EntityNotFoundException.class,
                () -> service.changeStatusById(userId, UUID.randomUUID().toString(), projectStatus)
        );
        Assert.assertThrows(
                EntityNotFoundException.class,
                () -> service.changeStatusById(UUID.randomUUID().toString(), project.getId(), projectStatus)
        );
        @Nullable Project projectChanged = service.changeStatusById(userId, project.getId(), projectStatus);
        Assert.assertNotNull(projectChanged);
        Assert.assertEquals(project.getId(), projectChanged.getId());
        Assert.assertEquals(projectStatus, projectChanged.getStatus());
    }

    @Test
    public void changeStatusByIndexAllEmpty() {
        @Nullable Project project = service.create(userId, projectName, projectDescription, dateBegin, dateEnd);
        Assert.assertNotNull(project);
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.changeStatusByIndex("", null, null)
        );
        Assert.assertThrows(
                IndexOutOfBoundsException.class,
                () -> service.changeStatusByIndex(userId, null, null)
        );
        Assert.assertThrows(
                IndexOutOfBoundsException.class,
                () -> service.changeStatusByIndex(userId, -50, null)
        );
        Assert.assertThrows(
                StatusEmptyException.class,
                () -> service.changeStatusByIndex(userId, 0, null)
        );
        Assert.assertThrows(
                StatusEmptyException.class,
                () -> service.changeStatusByIndex(UUID.randomUUID().toString(), 0, null)
        );
        @Nullable Project projectChanged = service.changeStatusByIndex(userId,0, projectStatus);
        Assert.assertNotNull(projectChanged);
        Assert.assertEquals(project.getId(), projectChanged.getId());
        Assert.assertEquals(projectStatus, projectChanged.getStatus());
    }

}
