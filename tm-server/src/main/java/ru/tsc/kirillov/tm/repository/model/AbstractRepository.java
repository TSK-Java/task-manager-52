package ru.tsc.kirillov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.api.repository.model.IRepository;
import ru.tsc.kirillov.tm.comparator.CreatedComparator;
import ru.tsc.kirillov.tm.comparator.DateBeginComparator;
import ru.tsc.kirillov.tm.comparator.StatusComparator;
import ru.tsc.kirillov.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.*;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final Class<M> clazz;

    @NotNull
    protected EntityManager entityManager;

    public AbstractRepository(@NotNull final Class<M> clazz, @NotNull final EntityManager entityManager) {
        this.clazz = clazz;
        this.entityManager = entityManager;
    }

    @NotNull
    protected String getColumnSort(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        else if (comparator == StatusComparator.INSTANCE) return "status";
        else if (comparator == DateBeginComparator.INSTANCE) return "status";
        else return "name";
    }

    @NotNull
    protected String getModelName() {
        return clazz.getSimpleName();
    }

    @Nullable
    protected M getResult(@NotNull final TypedQuery<M> query) {
        @NotNull final List<M> result = query.setMaxResults(1).getResultList();
        if (result.isEmpty()) return null;
        return result.get(0);
    }

    @Nullable
    @Override
    public M add(@NotNull final M model) {
        entityManager.persist(model);
        return model;
    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> models) {
        @NotNull List<M> result = new ArrayList<>();
        models
                .stream()
                .forEach(model -> result.add(add(model)));
        return result;
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) {
        clear();
        return add(models);
    }

    @Override
    public void clear() {
        removeAll(findAll());
    }

    @NotNull
    @Override
    public List<M> findAll() {
        @NotNull final String jpql = String.format("FROM %s", getModelName());
        return entityManager.createQuery(jpql, clazz).getResultList();
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final Comparator<M> comparator) {
        @NotNull final String jpql = String.format(
                "FROM %s ORDER BY %s",
                getModelName(),
                getColumnSort(comparator)
        );
        return entityManager.createQuery(jpql, clazz).getResultList();
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        @NotNull final String jpql = String.format("SELECT COUNT(m) = 1 FROM %s m WHERE m.id = :id", getModelName());
        return entityManager.createQuery(jpql, Boolean.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String id) {
        return entityManager.find(clazz, id);
    }

    @Nullable
    @Override
    public M findOneByIndex(@NotNull final Integer index) {
        @NotNull final String jpql = String.format("FROM %s", getModelName());
        @NotNull final TypedQuery<M> query = entityManager.createQuery(jpql, clazz).setFirstResult(index);
        return getResult(query);
    }

    @NotNull
    @Override
    public M remove(@NotNull final M model) {
        entityManager.remove(model);
        return model;
    }

    @Override
    public void removeAll(@Nullable final Collection<M> collection) {
        if (collection == null) return;
        collection
                .stream()
                .forEach(this::remove);
    }

    @Nullable
    @Override
    public M removeById(@NotNull final String id) {
        @NotNull final Optional<M> model = Optional.ofNullable(findOneById(id));
        model.ifPresent(this::remove);
        return model.orElse(null);
    }

    @Nullable
    @Override
    public M removeByIndex(@NotNull final Integer index) {
        @NotNull final Optional<M> model = Optional.ofNullable(findOneByIndex(index));
        model.ifPresent(this::remove);
        return model.orElse(null);
    }

    @Nullable
    @Override
    public M update(@NotNull final M model) {
        return entityManager.merge(model);
    }

    @Override
    public long count() {
        @NotNull final String jpql = String.format("SELECT COUNT(m) FROM %s m", getModelName());
        return entityManager.createQuery(jpql, Long.class).getSingleResult();
    }

}
