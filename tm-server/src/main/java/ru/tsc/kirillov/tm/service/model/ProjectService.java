package ru.tsc.kirillov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.api.repository.model.IProjectRepository;
import ru.tsc.kirillov.tm.api.service.IConnectionService;
import ru.tsc.kirillov.tm.api.service.model.IProjectService;
import ru.tsc.kirillov.tm.model.Project;
import ru.tsc.kirillov.tm.repository.model.ProjectRepository;

import javax.persistence.EntityManager;

public final class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    protected IProjectRepository getRepository(@NotNull final EntityManager entityManager) {
        return new ProjectRepository(entityManager);
    }

}
