package ru.tsc.kirillov.tm.component;

import org.jetbrains.annotations.NotNull;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class Backup {

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void start() {
        if (!bootstrap.getPropertyService().getBackupEnabled()) return;
        load();
        es.scheduleWithFixedDelay(this::save, 0, 3, TimeUnit.SECONDS);
    }

    public void stop() {
        es.shutdown();
    }

    public void save() {
        bootstrap.getDomainService().saveDataBackup();
    }

    public void load() {
        try {
            bootstrap.getDomainService().loadDataBackup();
        } catch (Exception e) {
            bootstrap.getLoggerService().info("Невозможно загрузить состояние приложение из бекапа: " + e.getMessage());
        }
    }

}
