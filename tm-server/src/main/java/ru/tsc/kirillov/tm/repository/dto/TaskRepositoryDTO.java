package ru.tsc.kirillov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.api.repository.dto.ITaskRepositoryDTO;
import ru.tsc.kirillov.tm.dto.model.TaskDTO;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

public class TaskRepositoryDTO extends AbstractUserOwnedRepositoryDTO<TaskDTO> implements ITaskRepositoryDTO {

    public TaskRepositoryDTO(@NotNull final EntityManager entityManager) {
        super(TaskDTO.class, entityManager);
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || projectId == null) return Collections.emptyList();
        @NotNull final String jpql = String.format(
                "FROM %s m WHERE userId = :userId AND m.projectId = :projectId",
                getModelName()
        );
        return entityManager.createQuery(jpql, clazz)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public void removeAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        @NotNull final String jpql = String.format(
                "DELETE FROM %s m WHERE m.userId = :userId AND m.projectId = :projectId",
                getModelName()
        );
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

    @Override
    public void removeAllByProjectList(@Nullable final String userId, @Nullable final String[] projects) {
        Stream.of(projects)
                .filter(p -> p != null && !p.isEmpty())
                .forEach(p -> removeAllByProjectId(userId, p));
    }

}
