package ru.tsc.kirillov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.api.repository.dto.ITaskRepositoryDTO;
import ru.tsc.kirillov.tm.api.service.IConnectionService;
import ru.tsc.kirillov.tm.api.service.dto.ITaskServiceDTO;
import ru.tsc.kirillov.tm.dto.model.TaskDTO;
import ru.tsc.kirillov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kirillov.tm.exception.field.ProjectIdEmptyException;
import ru.tsc.kirillov.tm.exception.field.UserIdEmptyException;
import ru.tsc.kirillov.tm.repository.dto.TaskRepositoryDTO;

import javax.persistence.EntityManager;
import java.util.List;

public class TaskServiceDTO extends AbstractUserOwnedServiceDTO<TaskDTO, ITaskRepositoryDTO> implements ITaskServiceDTO {

    public TaskServiceDTO(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    protected ITaskRepositoryDTO getRepository(@NotNull final EntityManager entityManager) {
        return new TaskRepositoryDTO(entityManager);
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepositoryDTO repository = getRepository(entityManager);
            return repository.findAllByProjectId(userId, projectId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeAllByProjectId(userId, projectId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAllByProjectList(@Nullable final String userId, @Nullable final String[] projects) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projects == null || projects.length == 0) throw new ProjectNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeAllByProjectList(userId, projects);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
