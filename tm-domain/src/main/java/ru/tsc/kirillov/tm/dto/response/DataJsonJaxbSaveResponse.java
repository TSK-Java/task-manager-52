package ru.tsc.kirillov.tm.dto.response;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class DataJsonJaxbSaveResponse extends AbstractResponse {
}
