package ru.tsc.kirillov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataYamlFasterXmlLoadRequest extends AbstractUserRequest {

    public DataYamlFasterXmlLoadRequest(@Nullable final String token) {
        super(token);
    }

}
