package ru.tsc.kirillov.tm.exception.field;

public final class TaskIdEmptyException extends IdEmptyException {

    public TaskIdEmptyException() {
        super("Ошибка! ID задачи не задано.");
    }

}
